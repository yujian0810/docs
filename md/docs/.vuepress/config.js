const { chainMarkdown, extendMarkdown } = require('./extendMarkdown');
const autometa_options = {
    site: {
      name: '鸿蒙内核源码分析',
      公众号: '鸿蒙内核源码分析',
    },
    canonical_base: 'https://weharmony.gitee.io',
  };
module.exports = {
    title: '热爱是所有的理由和答案',
    description: '鸿蒙 HarmonyOS 鸿蒙内核 鸿蒙系统 鸿蒙生态 OpenHarmony',
    plugins:[
            ['autometa', autometa_options],
            ['vuepress-plugin-baidu-autopush'],
            ['sitemap', {
                hostname: "https://weharmony.gitee.io",
                // 排除无实际内容的页面
                exclude: ["/404.html"]
              }
          ]
        ],
    head: [
        ['meta', { name: 'harmonyos', content: '百万汉字注解 百篇博客分析 鸿蒙内核源码 深挖地基工程'}],
        // 添加百度统计
        [
          "script",
          {},
          `
          var _hmt = _hmt || [];
          (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?b235e82afd6daba1ab12c42f15143f48";
            var s = document.getElementsByTagName("script")[0]; 
            s.parentNode.insertBefore(hm, s);
          })();
            `
        ]
      ],
      chainMarkdown,
      markdown: {
        lineNumbers: true,
        anchor: {
            permalink: false
        },
        toc: false,
        extendMarkdown
      }, 
    themeConfig: {
        nav: [
            {
                text: '进官方群',
                link: 'qun.html',
            },
            {
                text: '回首页',
                link: 'history.html', 
                ariaLabel: 'Language Menu',
                items: [
                  { text: '回首页', link: 'index.html' },
                  { text: '总目录', link: '08_总目录.html' },
                  { text: '更新记录', link: 'history.html' },
                  { text: '验证文档', link: '00_说明.html' },
                ]
            },
            {
                text: '资料下载',
                link: 'history.html',
            },
            {
                text: '故事',
                ariaLabel: 'Language Menu',
                items: [
                  { text: '调度故事篇', link: '09_调度故事篇.html' },
                  { text: '内存主奴篇', link: '10_内存主奴篇.html' },
                  { text: '源码注释篇', link: '13_源码注释篇.html' },
                ]
            },
            {
                text: 'ARM架构',
                ariaLabel: 'Language Menu',
                items: [
                  { text: '中断概念篇', link: '43_中断概念篇.html' },
                  { text: '中断管理篇', link: '44_中断管理篇.html' },
                  { text: '中断切换篇', link: '42_中断切换篇.html' },
                  { text: '汇编汇总篇', link: '40_汇编汇总篇.html' },
                  { text: '异常接管篇', link: '39_异常接管篇.html' },
                  { text: '寄存器篇', link: '38_寄存器篇.html' },
                  { text: '工作模式篇', link: '36_工作模式篇.html' },
                  { text: '系统调用篇', link: '37_系统调用篇.html' },
                  { text: 'CPU篇', link: '32_CPU篇.html' },
                  { text: '汇编基础篇', link: '22_汇编基础篇.html' },
                  { text: '汇编传参篇', link: '23_汇编传参篇.html' },
                ]
            },
            {
                text: '内存',
                ariaLabel: 'Language Menu',
                items: [
                  { text: '内存管理篇', link: '12_内存管理篇.html' },
                  { text: '内存汇编篇', link: '14_内存汇编篇.html' },
                  { text: '内存映射篇', link: '15_内存映射篇.html' },
                  { text: '内存规则篇', link: '16_内存规则篇.html' },
                  { text: '物理内存篇', link: '17_物理内存篇.html' },
                  { text: '内存分配篇', link: '11_内存分配篇.html' },
                ]
            },
            {
                text: '进程',
                ariaLabel: 'Language Menu',
                items: [
                    { text: '信号消费篇', link: '49_信号消费篇.html' },
                    { text: '信号生产篇', link: '48_信号生产篇.html' },
                    { text: '进程回收篇', link: '47_进程回收篇.html' },
                    { text: '特殊进程篇', link: '46_特殊进程篇.html' },
                    { text: 'Fork篇', link: '45_Fork篇.html' },
                    { text: '进程概念篇', link: '24_进程概念篇.html' },
                    { text: '进程管理篇', link: '02_进程管理篇.html' },
                ]
            },
            {
                text: '任务(线程)',
                ariaLabel: 'Language Menu',
                items: [
                    { text: '任务切换篇', link: '41_任务切换篇.html' },
                    { text: '并发并行篇', link: '25_并行并发篇.html' },
                    { text: '线程概念篇', link: '21_线程概念篇.html' },
                    { text: '时钟任务篇', link: '03_时钟任务篇.html' },
                    { text: '任务管理篇', link: '05_任务管理篇.html' },
                    { text: '任务调度篇', link: '04_任务调度篇.html' },
                    { text: '调度队列篇', link: '06_调度队列篇.html' },
                    { text: '调度机制篇', link: '07_调度机制篇.html' },
                ]
            },
            {
                text: '进程通讯',
                ariaLabel: 'Language Menu',
                items: [
                    { text: '进程通讯篇', link: '28_进程通讯篇.html' },
                    { text: '消息队列篇', link: '33_消息队列篇.html' },
                    { text: '事件控制篇', link: '30_事件控制篇.html' },
                    { text: '信号量篇', link: '29_信号量篇.html' },
                    { text: '互斥量篇', link: '27_互斥锁篇.html' },
                    { text: '自旋锁篇', link: '26_自旋锁篇.html' },
                ]
            },
            {
                text: '基础服务',
                ariaLabel: 'Language Menu',
                items: [
                  { text: 'ELF格式篇', link: '51_ELF格式篇.html' },
                  { text: '编译环境篇', link: '50_编译环境篇.html' },
                  { text: '时间管理篇', link: '35_时间管理篇.html' },
                  { text: '原子操作篇', link: '34_原子操作篇.html' },
                  { text: '定时器篇', link: '31_定时器篇.html' },
                  { text: '双向链表篇', link: '01_双向链表篇.html' },
                  { text: '用栈方式篇', link: '20_用栈方式篇.html' },
                  { text: '源码结构篇', link: '18_源码结构篇.html' },
                  { text: '位图管理篇', link: '19_位图管理篇.html' },
                ]
            },
            {
                text: '码仓',
                ariaLabel: 'Language Menu',
                items: [
                  { text: 'gitee', link: 'https://gitee.com/weharmony/kernel_liteos_a_note' },
                  { text: 'github', link: 'https://github.com/kuangyufei/kernel_liteos_a_note' },
                  { text: 'codechina', link: 'https://codechina.csdn.net/kuangyufei/kernel_liteos_a_note' },
                  { text: 'coding', link: 'https://weharmony.coding.net/public/harmony/kernel_liteos_a_note/git/files' }
                ]
              },
              {
                  text: '站点',
                  ariaLabel: 'Language Menu',
                  items: [
                    { text: 'oschina', link: 'https://my.oschina.net/weharmony/' },
                    { text: 'csdn', link: 'https://blog.csdn.net/kuangyufei/' },
                    { text: '51cto', link: 'https://harmonyos.51cto.com/column/34' },
                    { text: '掘金', link: 'https://juejin.cn/user/756888642000808' },
                    { text: '公众号', link: 'https://gitee.com/weharmony/docs/raw/master/pic/other/so1so.png' },
                    { text: '头条号', link: 'https://gitee.com/weharmony/docs/raw/master/pic/other/tt.png' },
                  ]
              },
              {
                text: '工具',
                ariaLabel: 'Language Menu',
                items: [
                  { text: '命令', link: 'https://www.linuxcool.com/' },
                  { text: '在线工具', link: 'https://tool.oschina.net/' },
                  { text: '在线工具', link: 'https://tool.lu/' },
                  { text: 'coding', link: 'https://weharmony.coding.net/public/harmony/kernel_liteos_a_note/git/files' }
                ]
              },
          ],
          sidebar: {
            "/": [
                {
                    title: '进官方群',
                    path: 'qun.html',
                    collapsable: false,
                },
                {
                    title: "资料下载",
                    path: 'history.html',
                    collapsable: false,
                }, 
                {
                    title: "总目录(100)",
                    path: '08_总目录.html',
                    collapsable: false,
                }, 
                {
                    title: "故事(3)",
                    collapsable: false,
                    children: [
                      { title: '调度故事篇', path: '09_调度故事篇.html' },
                      { title: '内存主奴篇', path: '10_内存主奴篇.html' },
                      { title: "源码注释篇",  path: "13_源码注释篇.html" },
                    ]
                },
                {
                    title: '进程(7)',
                    collapsable: false,
                    children: [
                        { title: '信号消费篇', path: '49_信号消费篇.html' },
                        { title: '信号生产篇', path: '48_信号生产篇.html' },
                        { title: '进程回收篇', path: '47_进程回收篇.html' },
                        { title: '特殊进程篇', path: '46_特殊进程篇.html' },
                        { title: 'Fork篇', path: '45_Fork篇.html' },
                        { title: '进程概念篇', path: '24_进程概念篇.html' },
                        { title: '进程管理篇', path: '02_进程管理篇.html' },
                    ]
                },
                {
                    title: '任务/线程(9)',
                    collapsable: false,
                    children: [
                        { title: '任务切换篇', path: '41_任务切换篇.html' },
                        { title: '并发并行篇', path: '25_并行并发篇.html' },
                        { title: '线程概念篇', path: '21_线程概念篇.html' },
                        { title: '时钟任务篇', path: '03_时钟任务篇.html' },
                        { title: '任务调度篇', path: '04_任务调度篇.html' },
                        { title: '任务管理篇', path: '05_任务管理篇.html' },
                        { title: '调度队列篇', path: '06_调度队列篇.html' },
                        { title: '调度机制篇', path: '07_调度机制篇.html' },
                        { title: 'CPU篇', path: '32_CPU篇.html' },
                    ]
                },
                {
                    title: 'ARM架构(10)',
                    collapsable: false,
                    children: [
                      { title: '中断概念篇', path: '43_中断概念篇.html' },
                      { title: '中断管理篇', path: '44_中断管理篇.html' },
                      { title: '中断切换篇', path: '42_中断切换篇.html' },
                      { title: '汇编汇总篇', path: '40_汇编汇总篇.html' },
                      { title: '异常接管篇', path: '39_异常接管篇.html' },
                      { title: '寄存器篇', path: '38_寄存器篇.html' }, 
                      { title: '工作模式篇', path: '36_工作模式篇.html' },
                      { title: '系统调用篇', path: '37_系统调用篇.html' },
                      { title: '汇编基础篇', path: '22_汇编基础篇.html' },
                      { title: '汇编传参篇', path: '23_汇编传参篇.html' },
                    ]
                },
              {
                title: "基础服务(9)",
                collapsable: false,
                children: [
                    { title: 'ELF格式篇', path: '51_ELF格式篇.html' },
                    { title: "编译环境篇",  path: "50_编译环境篇.html" },
                    { title: "时间管理篇",  path: "35_时间管理篇.html" },
                    { title: "原子操作篇",  path: "34_原子操作篇.html" },
                    { title: "定时器篇",  path: "31_定时器篇.html" },
                    { title: "双向链表篇",  path: "01_双向链表篇.html" },
                    { title: "用栈方式篇",  path: "20_用栈方式篇.html" },
                    { title: "源码结构篇",  path: "18_源码结构篇.html" },
                    { title: "位图管理篇",  path: "19_位图管理篇.html" },
                 ],
              },
              {
                title: "通讯方式(6)",
                collapsable: false,
                children: [
                  { title: '进程通讯篇', path: '28_进程通讯篇.html' },
                  { title: "消息队列篇",  path: "33_消息队列篇.html" },
                  { title: "事件控制篇",  path: "30_事件控制篇.html" },
                  { title: '信号量篇', path: '29_信号量篇.html' },
                  { title: '互斥量篇', path: '27_互斥锁篇.html' },
                  { title: '自旋锁篇', path: '26_自旋锁篇.html' },
                ],
              },
            {
                title: '虚实内存(6)',
                collapsable: false,
                children: [
                  { title: '内存管理篇', path: '12_内存管理篇.html' },
                  { title: '内存汇编篇', path: '14_内存汇编篇.html' },
                  { title: '内存映射篇', path: '15_内存映射篇.html' },
                  { title: '内存规则篇', path: '16_内存规则篇.html' },
                  { title: '物理内存篇', path: '17_物理内存篇.html' },
                  { title: '内存分配篇', path: '11_内存分配篇.html' },
                ]
            },
        ],
    },
    sidebarDepth: 3,
  },
}
