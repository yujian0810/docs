
```mermaid
sequenceDiagram
    autonumber
    Note right of 用户模式: main -> mq_open
    Note right of 用户模式: mq_open -> syscall
    Note right of 用户模式: syscall -> __asm_syscall
    用户模式->>_osExceptSwiHdl: svc 0
    Note right of _osExceptSwiHdl: SVC将用户模式切到内核模式
    _osExceptSwiHdl->>内核模式 :OsArmA32SyscallHandle
    Note right of 内核模式: 调用SYS_mq_open
    内核模式-->>_osExceptSwiHdl :SYS_mq_open执行完return
    _osExceptSwiHdl-->>用户模式:MSR     SPSR_cxsf, R3
    Note right of _osExceptSwiHdl: 切回用户模式
    _osExceptSwiHdl-->>用户模式:LDMFD   SP!, {PC}^ 
    Note right of _osExceptSwiHdl: 重新指向用户模式代码段
```

```mermaid
sequenceDiagram
    autonumber
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```
```mermaid
pie
    title Key elements in Product X
    "Calcium" : 42.96
    "Potassium" : 50.05
    "Magnesium" : 10.01
    "Iron" :  5
```

```mermaid
graph LR
A[长方形] -- 链接 --> B((圆))
A --> C(圆角长方形)
B --> D{菱形}
C --> D
```
```mermaid
sequenceDiagram
张三 ->> 李四: 你好！李四, 最近怎么样?
李四-->>王五: 你最近怎么样，王五？
李四--x 张三: 我很好，谢谢!
李四-x 王五: 我很好，谢谢!
Note right of 王五: 李四想了很长时间, 文字太长了<br/>不适合放在一行.

李四-->>张三: 打量着王五...
张三->>王五: 很好... 王五, 你怎么样?
```

```mermaid
flowchat
st=>start: 开始
e=>end: 结束
op=>operation: 我的操作
cond=>condition: 确认？

st->op->cond
cond(yes)->e
cond(no)->op
```

```mermaid
classDiagram
    Class01 <|-- AveryLongClass : Cool
    <<interface>> Class01
    Class09 --> C2 : Where am i?
    Class09 --* C3
    Class09 --|> Class07
    Class07 : equals()
    Class07 : Object[] elementData
    Class01 : size()
    Class01 : int chimp
    Class01 : int gorilla
    class Class10 {
        >>service>>
        int id
        size()
    }
```
