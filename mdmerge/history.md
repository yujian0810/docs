
[![在这里插入图片描述](https://gitee.com/weharmony/docs/raw/master/pic/other/io.png)](https://gitee.com/weharmony/kernel_liteos_a_note)
### 资料下载地址 

* 微云      [https://share.weiyun.com/cnIIp8WC](https://share.weiyun.com/cnIIp8WC) 密码：**73vwey**
* 百度      [https://pan.baidu.com/s/1U3SyVZyOYpGZxUQ7zLu6MQ](https://pan.baidu.com/s/1U3SyVZyOYpGZxUQ7zLu6MQ) 提取码: **2g24**

* 若无法下载,请入群/关注公众号,这样资料最新,最稳妥.
  
### 资料更新记录

请对比您手中的 **鸿蒙内核源码分析(百篇博客分析.挖透鸿蒙内核).pdf** 是否为最新版

版本更新时间:
* **`2021/05/01`**
* **`2021/04/29`**
* **`2021/04/27`** 

### 鸿蒙内核源码分析交流群
[![鸿蒙内核源码分析](https://gitee.com/weharmony/docs/raw/master/pic/other/qun.jpg)](https://gitee.com/weharmony/docs/raw/master/pic/other/qun.jpg)

!import[\mdmerge\foot.md]
!export[\md\docs\history.md]
