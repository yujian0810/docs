#include <stdio.h>
#include <stdarg.h>
#include "turing.h"

//获取指定结构体内的成员相对于结构体起始地址的偏移量
#define LOS_OFF_SET_OF(type, member) ((UINTPTR)&((type *)0)->member)

#define LOS_OFF_SET_OF1(type, member) ((int)&((type *)0)->member)

//获取包含链表的结构体地址，接口的第一个入参表示的是链表中的某个节点，第二个入参是要获取的结构体名称，第三个入参是链表在该结构体中的名称
#define LOS_DL_LIST_ENTRY(item, type, member) \
    ((type *)(void *)((char *)(item) - LOS_OFF_SET_OF(type, member)))

typedef struct Test{
    LOS_DL_LIST* pa;
    LOS_DL_LIST* pb;
    LOS_DL_LIST lc;
    LOS_DL_LIST ld;
    int a;
    int b;
}Test;

int main(){
    Test aa;
    aa.a = 90;
    aa.b = 89;
    printf("%d,%d,%d\n",LOS_OFF_SET_OF(Test,pa),LOS_OFF_SET_OF(Test,lc),LOS_OFF_SET_OF(Test,a));

    Test *pt = LOS_DL_LIST_ENTRY(aa.pb,Test,pb);
    printf("%d,%d\n",&aa,&pt);
    printf("%d,%d\n",pt->a,pt->b);
    return aa.a + aa.b;
}
// gcc -o list list.c
