
#include <stdio.h>
#include <math.h>
#include "turing.h"

static char *NextName(char *pos, UINT8 *len)
{
    char *name = NULL;
    if (*pos == '\0') {
        return NULL;
    }
    while (*pos != 0 && *pos == '/') {
        pos++;
    }
    if (*pos == '\0') {
        return NULL;
    }
    name = (char *)pos;
    while (*pos != '\0' && *pos != '/') {
        pos++;
    }
    *len = pos - name;
    return name;
}

int main()
{
    UINT8 len = 0;
    printf("NextName = %s ,%d \n",NextName("/dev/jj/jjjj",&len),len);
}